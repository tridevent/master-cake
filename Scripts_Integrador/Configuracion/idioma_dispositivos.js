﻿var Movil:GameObject;
var Tablet:GameObject;
var ingles:GameObject;
var espanol:GameObject;
//--------------------------------------------------------
var MovilActivado:Texture;
var TabletActivado:Texture;
var inglesActivado:Texture;
var espanolActivado:Texture;
//-----------------------------
var MovilDesactivado:Texture;
var TabletDesactivado:Texture;
var inglesDesactivado:Texture;
var espanolDesactivado:Texture;
private var BotonPulsado : boolean = false;
//--------------------Movil----------------------------------
private var yE:float=0;
private var xP:float=0;
private var yP:float=0;
//----------------Tablet--------------------------------------
private var yE2:float=0;
private var xP2:float=0;
private var yP2:float=0;
//----------------ingles--------------------------------------
private var yE3:float=0;
private var xP3:float=0;
private var yP3:float=0;
//----------------espanol--------------------------------------
private var yE4:float=0;
private var xP4:float=0;
private var yP4:float=0;
//------------------------------------------------------

function Start () {
//-------------------------------------------------------
yE=Movil.guiTexture.pixelInset.width;
xP=Movil.guiTexture.pixelInset.x;
yP=Movil.guiTexture.pixelInset.y;
//--------------------------------------------------------
yE2=Tablet.guiTexture.pixelInset.width;
xP2=Tablet.guiTexture.pixelInset.x;
yP2=Tablet.guiTexture.pixelInset.y;
//--------------------------------------------------------
yE3=ingles.guiTexture.pixelInset.width;
xP3=ingles.guiTexture.pixelInset.x;
yP3=ingles.guiTexture.pixelInset.y;
//--------------------------------------------------------
yE4=espanol.guiTexture.pixelInset.width;
xP4=espanol.guiTexture.pixelInset.x;
yP4=espanol.guiTexture.pixelInset.y;
//--------------------------------------------------------
xP=global.ancho*(xP)/456;
yP=global.alto*(yP)/608;
yE=global.ancho*(yE)/456;
//--------------------------------------------------------
xP2=global.ancho*(xP2)/456;
yP2=global.alto*(yP2)/608;
yE2=global.ancho*(yE2)/456;
//--------------------------------------------------------
xP3=global.ancho*(xP3)/456;
yP3=global.alto*(yP3)/608;
yE3=global.ancho*(yE3)/456;
//--------------------------------------------------------
xP4=global.ancho*(xP4)/456;
yP4=global.alto*(yP4)/608;
yE4=global.ancho*(yE4)/456;
//------------------------------------------------------------
Movil.guiTexture.pixelInset = Rect(xP,yP,yE,yE);
Tablet.guiTexture.pixelInset = Rect(xP2,yP2,yE2,yE2);
ingles.guiTexture.pixelInset = Rect(xP3,yP3,yE3,yE3);
espanol.guiTexture.pixelInset = Rect(xP4,yP4,yE4,yE4);

if(Seleccion_Dispositivos.DispositivoActual==0)
	{
	Movil.guiTexture.texture= MovilActivado;
	Tablet.guiTexture.texture= TabletDesactivado;
	}
if(Seleccion_Dispositivos.DispositivoActual==1)
	{
	Movil.guiTexture.texture= MovilDesactivado;
	Tablet.guiTexture.texture= TabletActivado;
	}
if(seleccionIdiomas.idiomaActual==0)
	{
	ingles.guiTexture.texture= inglesActivado;
	espanol.guiTexture.texture= espanolDesactivado;
	}
if(seleccionIdiomas.idiomaActual==1)
	{
	ingles.guiTexture.texture= inglesDesactivado;
	espanol.guiTexture.texture= espanolActivado;
	}
}

function Update () {

	if(Input.touchCount > 0)
	    {
	        for (var i = 0; i < Input.touchCount; ++i) 
	        {   
	            if(Movil.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                Seleccion_Dispositivos.DispositivoActual=0;
	                BotonPulsado = true;                         	           
	            }
	            if(Tablet.guiTexture.HitTest(Input.GetTouch(i).position))
	            {       
		    		Seleccion_Dispositivos.DispositivoActual=1;
	                BotonPulsado = true;
	                                        	           
	            }
	            if(ingles.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
		    		seleccionIdiomas.idiomaActual=0;
	                BotonPulsado = true;    	           
	            }
	            if(espanol.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
		    		seleccionIdiomas.idiomaActual=1;
	                BotonPulsado = true;                       	           
	            }
	        } 
	    } 
		if (Input.GetMouseButton(0))
		{
	    	if (Movil.guiTexture.HitTest(Input.mousePosition))
	    	{
		    		Seleccion_Dispositivos.DispositivoActual=0;
	                BotonPulsado = true;
	    	}
	    	if (Tablet.guiTexture.HitTest(Input.mousePosition))
	    	{
		    		Seleccion_Dispositivos.DispositivoActual=1;
	                BotonPulsado = true;
	    	}
	    	if (ingles.guiTexture.HitTest(Input.mousePosition))
	    	{
		    		seleccionIdiomas.idiomaActual=0;
	                BotonPulsado = true;
	    	}
	    	if (espanol.guiTexture.HitTest(Input.mousePosition))
	    	{
		    		seleccionIdiomas.idiomaActual=1;
	                BotonPulsado = true;
	    	}
	    }
	    if(BotonPulsado)
	    {
		configuracionGUI.config=true;
		if(Seleccion_Dispositivos.DispositivoActual==0)
			{
			Movil.guiTexture.texture= MovilActivado;
			Tablet.guiTexture.texture= TabletDesactivado;
			}
		if(Seleccion_Dispositivos.DispositivoActual==1)
			{
			Movil.guiTexture.texture= MovilDesactivado;
			Tablet.guiTexture.texture= TabletActivado;
			}
		if(seleccionIdiomas.idiomaActual==0)
			{
			ingles.guiTexture.texture= inglesActivado;
			espanol.guiTexture.texture= espanolDesactivado;
			}
		if(seleccionIdiomas.idiomaActual==1)
			{
			ingles.guiTexture.texture= inglesDesactivado;
			espanol.guiTexture.texture= espanolActivado;
			}	
	    }          		    	
	    BotonPulsado = false;
	
}





