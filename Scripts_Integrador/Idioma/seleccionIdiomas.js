﻿static var idiomaActual:int=0; //0 ingles 1 español
var ingles:GameObject;
var espanol:GameObject;
//--------------------------------------------------------
var inglesActivado:Texture;
var espanolActivado:Texture;
private var BotonPulsado : boolean = false;
var nivel:String;
//--------------------ingles----------------------------------
private var yE:float=0;
private var xP:float=0;
private var yP:float=0;
//----------------español--------------------------------------
private var yE2:float=0;
private var xP2:float=0;
private var yP2:float=0;
//------------------------------------------------------

function Start () {
//-------------------------------------------------------
yE=ingles.guiTexture.pixelInset.width;
xP=ingles.guiTexture.pixelInset.x;
yP=ingles.guiTexture.pixelInset.y;
//--------------------------------------------------------
yE2=espanol.guiTexture.pixelInset.width;
xP2=espanol.guiTexture.pixelInset.x;
yP2=espanol.guiTexture.pixelInset.y;
//--------------------------------------------------------
xP=global.ancho*(xP)/456;
yP=global.alto*(yP)/608;
yE=global.ancho*yE/456;
//--------------------------------------------------------
xP2=global.ancho*(xP2)/456;
yP2=global.alto*(yP2)/608;
yE2=global.ancho*yE2/456;
//------------------------------------------------------------
ingles.guiTexture.pixelInset = Rect(xP,yP,yE,yE);
espanol.guiTexture.pixelInset = Rect(xP2,yP2,yE2,yE2);
}

function Update () {

	if(Input.touchCount > 0)
	    {
	        for (var i = 0; i < Input.touchCount; ++i) 
	        {   
	            if(ingles.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                //BotonPulsado = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=0;
	                ActivarIngles();
	                }                          	           
	            }
	            if(espanol.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                //BotonPulsado = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=1;
	                ActivarEspanol();
	                }                          	           
	            }
	        } 
	    } 
		if (Input.GetMouseButton(0))
		{
	    	if (ingles.guiTexture.HitTest(Input.mousePosition))
	    	{
	    	//BotonPulsado = true;
	    	if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=0;
	                ActivarIngles();
	                } 
	    	}
	    	if (espanol.guiTexture.HitTest(Input.mousePosition))
	    	{
	    	//BotonPulsado = true;
	    	if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=1;
	                ActivarEspanol();
	                } 
	    	}
	    }
	    if(BotonPulsado)
	    {
		Application.LoadLevel(nivel);	
	    }          		    	
	    BotonPulsado = false;
	
}
function ActivarIngles()
{
ingles.guiTexture.texture= inglesActivado;
yield WaitForSeconds (0.3);
BotonPulsado = true;
}
function ActivarEspanol()
{
espanol.guiTexture.texture= espanolActivado;
yield WaitForSeconds (0.3);
BotonPulsado = true;
}




