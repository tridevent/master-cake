﻿static var DispositivoActual:int=0; //0 Movil 1 español
var Movil:GameObject;
var Tablet:GameObject;
//--------------------------------------------------------
var MovilActivado:Texture;
var TabletActivado:Texture;
private var BotonPulsado : boolean = false;
var nivel:String;
//--------------------Movil----------------------------------
private var yE:float=0;
private var xP:float=0;
private var yP:float=0;
//----------------español--------------------------------------
private var yE2:float=0;
private var xP2:float=0;
private var yP2:float=0;
//------------------------------------------------------

function Start () {
//-------------------------------------------------------
yE=Movil.guiTexture.pixelInset.width;
xP=Movil.guiTexture.pixelInset.x;
yP=Movil.guiTexture.pixelInset.y;
//--------------------------------------------------------
yE2=Tablet.guiTexture.pixelInset.width;
xP2=Tablet.guiTexture.pixelInset.x;
yP2=Tablet.guiTexture.pixelInset.y;
//--------------------------------------------------------
xP=global.ancho*(xP)/456;
yP=global.alto*(yP)/608;
yE=global.ancho*yE/456;
//--------------------------------------------------------
xP2=global.ancho*(xP2)/456;
yP2=global.alto*(yP2)/608;
yE2=global.ancho*yE2/456;
//------------------------------------------------------------
Movil.guiTexture.pixelInset = Rect(xP,yP,yE,yE);
Tablet.guiTexture.pixelInset = Rect(xP2,yP2,yE2,yE2);
}

function Update () {

	if(Input.touchCount > 0)
	    {
	        for (var i = 0; i < Input.touchCount; ++i) 
	        {   
	            if(Movil.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                //BotonPulsado = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
		    		DispositivoActual=0;
	                ActivarMovil();
	                }                          	           
	            }
	            if(Tablet.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                //BotonPulsado = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
		    		DispositivoActual=1;
	                ActivarTablet();
	                }                          	           
	            }
	        } 
	    } 
		if (Input.GetMouseButton(0))
		{
	    	if (Movil.guiTexture.HitTest(Input.mousePosition))
	    	{
	    	//BotonPulsado = true;
	    	if(fondo_DobleTouch.temporizador>0)
		    		{
		    		DispositivoActual=0;
	                ActivarMovil();
	                } 
	    	}
	    	if (Tablet.guiTexture.HitTest(Input.mousePosition))
	    	{
	    	//BotonPulsado = true;
	    	if(fondo_DobleTouch.temporizador>0)
		    		{
		    		DispositivoActual=1;
	                ActivarTablet();
	                } 
	    	}
	    }
	    if(BotonPulsado)
	    {
		Application.LoadLevel(nivel);	
	    }          		    	
	    BotonPulsado = false;
	
}
function ActivarMovil()
{
Movil.guiTexture.texture= MovilActivado;
yield WaitForSeconds (0.3);
BotonPulsado = true;
}
function ActivarTablet()
{
Tablet.guiTexture.texture= TabletActivado;
yield WaitForSeconds (0.3);
BotonPulsado = true;
}




